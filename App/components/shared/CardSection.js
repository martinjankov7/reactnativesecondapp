import React from 'react';
import { View } from 'react-native';

const CardSection = ({ children }) => {
	return (
		<View style={styles.viewStyle}>
			{children}
		</View>
	);
}

const styles = {
	viewStyle : {
		borderColor: '#ccc',
		borderBottomWidth: 1,
		padding: 5,
		backgroundColor: '#fff',
		flexDirection: 'row'
	}
}

export default CardSection;