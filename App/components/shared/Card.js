import React from 'react';
import { View } from 'react-native';

const Card = ({ children }) => {
	return (
		<View style={styles.viewStyle}>
			{children}
		</View>
	);
}

const styles = {
	viewStyle : {
		marginTop: 10,
		marginBottom: 10,
		marginLeft: 10,
		marginRight: 10,
		borderWidth: 1,
		borderColor: '#ccc',
		borderRadius: 2,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2
		},
		shadowOpacity: 0.2,
		shadowRadius: 2,
		borderBottomWidth: 0
	}
}

export default Card;