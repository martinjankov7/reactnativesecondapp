import React, { Component } from 'react';
import { View, Text, Button, Alert, AsyncStorage, ListView } from 'react-native';
import CardSection from '../components/shared/CardSection';

export default class Row extends Component {
	constructor(props){
		super(props);
	}

	showAlert(){
		Alert.alert(
	      'Delete',
	      'To be or not to be',
	      [
	        { text: 'To Be', onPress: () => this.deleteUser(this.props.id) },
	        { text: 'Not To Be', onPress: () => {}, style: 'cancel' },
	      ],
	      { cancelable: true }
	    );
	}

	async deleteUser(id){
		let users = await AsyncStorage.getItem('users');
		users = JSON.parse(users);
		users = users.filter(function( user ) {
		    return user.id !== id;
		});

		this.props.update(users);
	}

	render(){
		return  (
			<CardSection>
			    <Text style={styles.name}>{this.props.name}</Text>
			    <Button style={styles.button} onPress={this.showAlert.bind(this)} title="Click Me" color="#4C6BFC" />
			</CardSection>
		);
	}
}
const styles = {
  name: {
  	flex: 2,
    marginLeft: 5,
    fontSize: 20,
  },
  button: {
  	flex: 1
  }
}

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});