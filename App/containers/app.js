import React, { Component } from 'react';
import { View, ListView, AsyncStorage } from 'react-native';
import Header from '../components/shared/Header';
import Card from '../components/shared/Card';
import Spinner from '../components/shared/Spinner';
import Row from '../components/Row';

export default class ReactNativeSecondApp extends Component {
  constructor(){
    super();

    this.state = {
      users : {},
      loading: true
    };
  }

  getUsers(){
    fetch('https://jsonplaceholder.typicode.com/users', {
          method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
      }).then((response) => {
          response.json().then((data) => {
            let userInfo = []; 
            data.map(function(user){
              userInfo.push(
                user
              );
            });
            this.updateUsers(userInfo);
          });
      });
  }

  updateUsers(userInfo){
    if(userInfo.length){ 
      AsyncStorage.setItem('users', JSON.stringify(userInfo), () => {});
      userData = ds.cloneWithRows(userInfo);
      this.setState({
        users: userData,  
        loading: false
      });
    }
    else this.getUsers();

  }

  async componentWillMount(){
    let users = await AsyncStorage.getItem('users');

    if (JSON.parse(users).length) {
        this.setState({
          users: ds.cloneWithRows(JSON.parse(users)),
          loading: false
        });
      return;
    }
  }

  render() {
    if(this.state.loading){
      return <Spinner size='large' />
    }
   
    return (
      <View>
        <Header headerText="Second Application"/>
        <Card>
            <ListView
                    dataSource={this.state.users}
                    renderRow={(data) => <Row name={data.name} id={data.id} update={this.updateUsers.bind(this)}/>}
                  />
        </Card>
      </View>
    );
  }
}

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});