import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import ReactNativeSecondApp from './App/containers/app';

AppRegistry.registerComponent('ReactNativeSecondApp', () => ReactNativeSecondApp);